//TMP36 Pin Variables
int sensorPin = 0; //the analog pin the TMP36's Vout (sense) pin is connected to
                        //the resolution is 10 mV / degree centigrade with a
                        //500 mV offset to allow for negative temperatures

const int ledPin = 13;

/*
 * setup() - this function runs once when you turn your Arduino on
 * We initialize the serial connection with the computer
 */
void setup()
{
  Serial.begin(9600);  //Start the serial connection with the computer
                       //to view the result open the serial monitor 

  // initialize the digital pin as an output.
  pinMode(ledPin, OUTPUT);                     
}
 
void loop()                     // run over and over again
{
  digitalWrite(ledPin, HIGH);   // set the LED on
  delay(1000);                  // wait for a second
  
 //getting the voltage reading from the temperature sensor
 int reading = analogRead(sensorPin);  
 
 // converting that reading to voltage, 
 // for 5.0v arduino use 5.0 
 // for 3.3v arduino use 3.3
 float voltage = reading * 3.3;
 voltage /= 1024.0; 

 
 // now print out the temperature
 float temperatureC = (voltage - 0.5) * 100 ;  //converting from 10 mv per degree wit 500 mV offset
                                               //to degrees ((voltage - 500mV) times 100)
 
 // now convert to Fahrenheit
 //float tempF = (temperatureC * 9.0 / 5.0) + 32.0;

 //temp = temperatureC;

  String cosmos_value =  "{\"device_tsen_temp_000\":" 
            + String(temperatureC) 
            + "}";

//  String cosmos_value1 = "{\"device_tsen_temp_000\":450.94}";

  //Serial.println(String(temperatureC));
  Serial.println(cosmos_value);
  Serial.println(".");
  
  digitalWrite(ledPin, LOW);    // set the LED off   
  delay(1000);                  //waiting a second
}
